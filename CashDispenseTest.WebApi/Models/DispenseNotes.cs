﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CashDispenseTest.Models
{
    public class DispenseNotes
    {
        int NumberOf50Pound { get; set; }
        int NumberOf20Pound { get; set; }
        int NumberOf10Pound { get; set; }
        int NumberOf5Pound { get; set; }
        int NumberOf2Pound { get; set; }
        int NumberOf1Pound { get; set; }
        int NumberOf50Pence { get; set; }
        int NumberOf20Pence { get; set; }
        int NumberOf10Pence { get; set; }
        int NumberOf5Pence { get; set; }
        int NumberOf2Pence { get; set; }
        int NumberOf1Pence { get; set; }
    }
}