﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CashDispenseTest.WebApi.Models
{
    public class DispenseCashResponseModel
    {
        public string CurrencyAmount { get; set; }
        public string Number { get; set; }
    }
}