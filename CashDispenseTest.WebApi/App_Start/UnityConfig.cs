using Microsoft.Practices.Unity;
using System.Web.Http;
using Unity.WebApi;
using CashDispenseTest.Repos;


namespace CashDispenseTest
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();

            container.RegisterType<IDispenseCashRepo, DispenseCashRepo>(new HierarchicalLifetimeManager()); 
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);
        }
    }
}