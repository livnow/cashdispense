﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CashDispenseTest.Repos
{
    public class DispenseCashRepo : IDispenseCashRepo
    {
        public Dictionary<double, int> GetAvailableDenominations()
        {

            Dictionary<double, int> availableDenominations = new Dictionary<double, int>();
            //Add the initial deposit here
            availableDenominations.Add(50, 50);
            availableDenominations.Add(20, 50);
            availableDenominations.Add(10, 50);
            availableDenominations.Add(5, 50);
            availableDenominations.Add(2, 100);
            availableDenominations.Add(1, 100);
            availableDenominations.Add(0.5, 100);
            availableDenominations.Add(0.2, 100);
            availableDenominations.Add(0.1, 100);
            availableDenominations.Add(0.05, 100);
            availableDenominations.Add(0.02, 100);
            availableDenominations.Add(0.01, 100);


            return availableDenominations;
        }



        public Dictionary<double, int> GetAvailableDenominations02()
        {

            Dictionary<double, int> availableDenominations = new Dictionary<double, int>();
            //Add the initial deposit here
            availableDenominations.Add(50, 50);
            availableDenominations.Add(20, 50);
            availableDenominations.Add(10, 50);
            availableDenominations.Add(5, 50);
            availableDenominations.Add(2, 100);
            availableDenominations.Add(1, 100);
            availableDenominations.Add(0.5, 100);
            availableDenominations.Add(0.2, 100);
            availableDenominations.Add(0.1, 100);
            availableDenominations.Add(0.05, 100);
            availableDenominations.Add(0.02, 100);
            availableDenominations.Add(0.01, 100);


            return availableDenominations;
        }

        public double GetBalance(Dictionary<double, int> availableDenominations)
        {
            var balance = 0.0;
            foreach (var item in availableDenominations)
            {
                if (item.Value > 0)
                {
                    balance = balance + (item.Key * item.Value);
                }
            }
            return balance;
        }

        public Dictionary<double, int> UpdateGetAvailableDenominations()
        {
            throw new NotImplementedException();
        }
    }
}