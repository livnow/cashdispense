﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace CashDispenseTest.Repos
{
    public interface IDispenseCashRepo
    {

        Dictionary<double, int> GetAvailableDenominations();
        Dictionary<double, int> GetAvailableDenominations02();
        double GetBalance(Dictionary<double, int> availableDenominations);

        Dictionary<double, int> UpdateGetAvailableDenominations();
    }
}