﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using Microsoft.Practices.Unity;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http.Cors;
using CashDispenseTest.WebApi.Models;

namespace CashDispenseTest.Controllers
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    [RoutePrefix("api/cashDispense")]
    public class DispenseCashController : ApiController
    {


        private Repos.IDispenseCashRepo _DispenseCashRepo;
        public DispenseCashController(Repos.IDispenseCashRepo repo)
        {
            _DispenseCashRepo = GlobalConfiguration.Configuration.DependencyResolver.GetService(typeof(Repos.IDispenseCashRepo)) as Repos.IDispenseCashRepo;
        }


        [Route("Algo1")]
        public List<DispenseCashResponseModel> DispenseNotes(double withdrawAmount)
        {
            Dictionary<string, string> outputDispense = new Dictionary<string, string>();

            List<DispenseCashResponseModel> responseList = new List<DispenseCashResponseModel>();

            var availableDenominations = _DispenseCashRepo.GetAvailableDenominations();

            var input = withdrawAmount;


            if (input > _DispenseCashRepo.GetBalance(availableDenominations))
            {
                var notEnoughBalance = new DispenseCashResponseModel
                {
                    CurrencyAmount = "Not Enough Balance.Your Current Balance is",
                    Number = "£" + _DispenseCashRepo.GetBalance(availableDenominations).ToString()
                };

                responseList.Add(notEnoughBalance);

                return responseList;

            }

            foreach (var item in availableDenominations.ToList())
            {
                //cheeck if the denomination note is availalb
                if (input >= item.Key & item.Value > 0)
                {
                    var dispenseDenominations = input / item.Key;
                    var reminder = input % item.Key;

                    //Check if the machine has the available notes
                    var requestedNotes = Math.Truncate(dispenseDenominations);

                    //update the available notes
                    var balanceNotes = item.Value - (int)requestedNotes;
                    if (balanceNotes < 0)
                    {
                        //balance notes is more than requested notes
                        //we are dispensing all teh notes for the size. So update the dispense denominations
                        dispenseDenominations = availableDenominations[item.Key];

                        //Since we dispensed all the available notes for the size, none left. update the count
                        availableDenominations[item.Key] = 0;

                        reminder = (reminder + availableDenominations[item.Key] * Math.Abs(balanceNotes));
                    }
                    else
                    {
                        availableDenominations[item.Key] = availableDenominations[item.Key] - (int)requestedNotes;
                    }
                    outputDispense.Add(item.Key.ToString(), Math.Truncate(dispenseDenominations).ToString());
                    input = reminder;
                }

            }

            foreach (var item in outputDispense)
            {
                var responseObj = new DispenseCashResponseModel
                {
                    CurrencyAmount = "£" + item.Key,
                    Number = item.Value
                };

                responseList.Add(responseObj);

            }

            var responseBalanceObj = new DispenseCashResponseModel
            {
                CurrencyAmount = "Balance Amount",
                Number = "£" + _DispenseCashRepo.GetBalance(availableDenominations).ToString()
            };

            responseList.Add(responseBalanceObj);

            return responseList;
        }

        [Route("Algo2")]
        public List<DispenseCashResponseModel> DispenseNotes1(double withdrawAmount)
        {
            Dictionary<string, string> outputDispense = new Dictionary<string, string>();

            List<DispenseCashResponseModel> responseList = new List<DispenseCashResponseModel>();

            var availableDenominations = _DispenseCashRepo.GetAvailableDenominations02();

            var input = withdrawAmount;

            if (input > _DispenseCashRepo.GetBalance(availableDenominations))
            {
                var notEnoughBalance = new DispenseCashResponseModel
                {
                    CurrencyAmount = "Not Enough Balance.Your Current Balance is",
                    Number = "£" + _DispenseCashRepo.GetBalance(availableDenominations).ToString()
                };

                responseList.Add(notEnoughBalance);

                return responseList;

            }


            foreach (var item in availableDenominations.ToList())
            {
                //cheeck if the denomination note is availalb
                if (input >= item.Key & item.Value > 0)
                {
                    var dispenseDenominations = input / item.Key;
                    var reminder = input % item.Key;

                    //Check if the machine has the available notes
                    var requestedNotes = Math.Truncate(dispenseDenominations);

                    //update the available notes
                    var balanceNotes = item.Value - (int)requestedNotes;
                    if (balanceNotes < 0)
                    {
                        //balance notes is more than requested notes
                        //we are dispensing all teh notes for the size. So update the dispense denominations
                        dispenseDenominations = availableDenominations[item.Key];

                        //Since we dispensed all the available notes for the size, none left. update the count
                        availableDenominations[item.Key] = 0;

                        reminder = (reminder + availableDenominations[item.Key] * Math.Abs(balanceNotes));
                    }
                    else
                    {
                        availableDenominations[item.Key] = availableDenominations[item.Key] - (int)requestedNotes;
                    }
                    outputDispense.Add(item.Key.ToString(), Math.Truncate(dispenseDenominations).ToString());
                    input = reminder;
                }

            }

            //Write to console
            foreach (var item in outputDispense)
            {
                var responseObj = new DispenseCashResponseModel
                {
                    CurrencyAmount = "£" + item.Key,
                    Number = item.Value
                };

                responseList.Add(responseObj);

            }

            var responseBalanceObj = new DispenseCashResponseModel
            {
                CurrencyAmount = "Balance Amount",
                Number = "£" + _DispenseCashRepo.GetBalance(availableDenominations).ToString()
            };

            responseList.Add(responseBalanceObj);

            return responseList;
        }



    }
}