﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(CashDispenseTest.Web.Startup))]
namespace CashDispenseTest.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
