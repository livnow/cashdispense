﻿var app = angular.module('testWebApiApp', []);
app.controller('DispenseCashController', function ($scope, $http, $window) {
        $scope.dispenseCash = {};
        $scope.dispenseCash.result = [];
        $scope.message = '';
        //get called when user submits the form  
        $scope.submitAmount = function() {
            console.log('Form is submitted with:', $scope.dispenseCash);
            //$http service that send or receive data from the remote server  
            var selectedAlgo=0;
            var selectedMethod = $scope.dispenseCash.AlgoType;
            if (selectedMethod == 1) {
                selectedAlgo = "Algo1";
            } if (selectedMethod == 2) {
                selectedAlgo = "Algo2";
            } else {
                selectedAlgo = "Algo1";
            }
            var req = {
                method: 'POST',
                url: 'http://localhost:5252/api/cashDispense/' + selectedAlgo + '?withdrawAmount=' + $scope.dispenseCash.CashText,
                headers: {
                    'Access-Control-Allow-Origin': "*"
                }
            }
            $http(req).success(function (data, status, headers, config) {
                $scope.errors = [];
                debugger;
                $scope.dispenseCash.result = data;
            }).error(function (data, status, headers, config) {
                $scope.errors = [];
                $scope.message = 'Unexpected Error while saving data!!';
            });
            //$scope.isViewLoading = false;
        }
    
    });
